//var express = require('express');
//var user_file = require('../user.json');
var bodyParser = require('body-parser'); //aqui se parsea como json
const global = require('../global');
var app = global.express();
var port = process.env.Port || 3000;
//const URL_BASE = '/techu-peru/v1/'
app.use(bodyParser.json());

// Peticiòn GET
//app.get(global.URL_BASE + 'users',
 function getUsers(req, res) {
  //res.send({"msg":"Operaciòn Get exitosa"});
  //res.status(202).send(user_file);
  res.status(202);
  res.send(global.user_file);
};

//Peticion GET users con Id
//app.get(URL_BASE + 'users/:id/:otro/:nuevo',
//app.get(global.URL_BASE + 'users/:id',
function getUserId(req, res){
  let pos = req.params.id-1;
  console.log("GET con id = " + req.params.id);
  //--let tam = user_file.length;
  //--console.log(tam);
  //let respuesta = user_file[pos];
  let respuesta = (global.user_file[pos] == undefined) ?
    {"msg":"Usuario no encontrado"}:global.user_file[pos];
  res.send(respuesta);
  //console.log("GET con id = " + req.params.otro);
  //console.log("GET con id = " + req.params.nuevo);
};

//GET users con Query String
//app.get(URL_BASE + 'users',
//function (req,res){
//  console.log('GET con Query String');
//  console.log(req.query.id);
//  console.log(req.query.name);

//})

//POST de users
//app.post(global.URL_BASE + 'users',
  function postUsers(req, res){
    console.log('POST de users');
    //console.log('Nuevo usuario: '+ req.body);
    //  console.log('Nuevo usuario: '+ req.body.first_name);
    //  console.log('Nuevo usuario: '+ req.body.email);
    let newID = global.user_file.length + 1;
    let newUser = {
      "id" : newID,
      "first name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    }
    global.user_file.push(newUser); //añadir un nuevo usuario o nuevo objeto
    console.log("nuevo usuario: " + newUser);
    res.send(newUser);
    //res.send({"msg":"POST exitoso"})
  };

  //PUT de users
  //app.put(global.URL_BASE + 'users/:id',
    function putUserId(req, res){
      console.log('PUT de users');

      let posi = req.params.id;
      console.log("PUT con id = " + req.params.id);
      let newUser = {
        "id" : posi,
        "first name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      }
      global.user_file[posi - 1]= newUser
      console.log("usuario actualizado: " + newUser);
      res.send(newUser);
      //res.send({"msg":"POST exitoso"})
    };

//DELETE users
//app.delete(global.URL_BASE + 'users/:id',
function deleteUserId(req,res){
  console.log('Delete de users');
  //vamos a usar el splice
  let pos = req.params.id;
  global.user_file.splice(pos - 1,1);
  //if user_file[pos] == undefined
  //   {"msg":"Usuario no encontrado"}
  //else {
    res.send({"msg":"Usuario eliminado"});
  //}

};

//Login
//app.post(global.URL_BASE + 'login',
function loginUser(req,res){
  console.log('Login');
  console.log(req.body.email);
  console.log(req.body.password)
  let tam = global.user_file.length;
  let i= 0;
  let encontrado = false;
  let email = req.body.email;
  let password = req.body.password;

  while ((i < global.user_file.length) && !encontrado){
     if (user_file[i].email == email && user_file[i].password == password)
     {
       idusuario = global.user_file[i].id;
       global.user_file[i].logged = true;
       encontrado = true;
  //   //  break;
     }
     i++;
     if (encontrado)
       res.send({"encontrado":"si","id":i});
     else {
       res.send({"encontrado": "no"});
     }
   }
};

//Escribir datos en fichero persistente
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 };





//Logout
//app.post(global.URL_BASE + 'logout/:id',
function logoutUser(req,res){
  console.log('Logout');
  console.log(req.params.id);
  let i= 0;
  let encontrado = false;
  let pos = req.params.id - 1;


  if (global.user_file[pos].logged == true) {
    delete user_file[pos].logged;
    //if (user_file[pos].logged = false){
       res.send({"logout correcto":"si","id":req.params.id});
    }
  else {
        res.send({"logout incorrecto":"si","id":req.params.id});
  }

};

module.exports.getUsers = getUsers;
module.exports.getUserId = getUserId;
module.exports.postUsers = postUsers;
module.exports.putUserId = putUserId;
module.exports.deleteUserId = deleteUserId;
module.exports.loginUser = loginUser;
module.exports.logoutUser = logoutUser;

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
