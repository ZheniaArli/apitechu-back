//var express = require('express');
//var user_file = require('./user.json');
require('dotenv').config();
const cors = require('cors');
//var app= express();

var bodyParser = require('body-parser'); //aqui se parsea como json
const global = require('./global');
var app = global.express();
var port = process.env.PORT || 3003;
//const URL_BASE = '/techu-peru/v1/'

var requestJSON = require('request-json');
var client = requestJSON.createClient('http://localhost:3008/');
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu50db/collections/';

//const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const apikeyMLab = 'apiKey=' + process.env.MLAB_API_KEY;

const users_contraller = require ('./controllers/users');
app.use(cors());
app.options('*', cors());

app.use(bodyParser.json());

// Peticiòn GET
app.get(global.URL_BASE + 'users', users_contraller.getUsers);
//  function (req, res) {
//   //res.send({"msg":"Operaciòn Get exitosa"});
//   //res.status(202).send(user_file);
//   res.status(202);
//   res.send(global.user_file);
// });
//
//Peticion GET users con Id
//app.get(URL_BASE + 'users/:id/:otro/:nuevo',
app.get(global.URL_BASE + 'users/:id', users_contraller.getUserId);
// function (req, res){
//   let pos = req.params.id-1;
//   console.log("GET con id = " + req.params.id);
//   //--let tam = user_file.length;
//   //--console.log(tam);
//   //let respuesta = user_file[pos];
//   let respuesta = (user_file[pos] == undefined) ?
//     {"msg":"Usuario no encontrado"}:global.user_file[pos];
//   res.send(respuesta);
//   //console.log("GET con id = " + req.params.otro);
//   //console.log("GET con id = " + req.params.nuevo);
// });

//GET users con Query String
//app.get(URL_BASE + 'users',
//function (req,res){
//  console.log('GET con Query String');
//  console.log(req.query.id);
//  console.log(req.query.name);

//})

//POST de users
app.post(global.URL_BASE + 'users', users_contraller.postUsers);
  // function(req, res){
  //   console.log('POST de users');
  //   //console.log('Nuevo usuario: '+ req.body);
  //   //  console.log('Nuevo usuario: '+ req.body.first_name);
  //   //  console.log('Nuevo usuario: '+ req.body.email);
  //   let newID = global.user_file.length + 1;
  //   let newUser = {
  //     "id" : newID,
  //     "first name" : req.body.first_name,
  //     "last_name" : req.body.last_name,
  //     "email" : req.body.email,
  //     "password" : req.body.password
  //   }
  //   global.user_file.push(newUser); //añadir un nuevo usuario o nuevo objeto
  //   console.log("nuevo usuario: " + newUser);
  //   res.send(newUser);
  //   //res.send({"msg":"POST exitoso"})
  // });

  //PUT de users
  app.put(global.URL_BASE + 'users/:id', users_contraller.putUserId);
    // function(req, res){
    //   console.log('PUT de users');
    //
    //   let posi = req.params.id;
    //   console.log("PUT con id = " + req.params.id);
    //   let newUser = {
    //     "id" : posi,
    //     "first name" : req.body.first_name,
    //     "last_name" : req.body.last_name,
    //     "email" : req.body.email,
    //     "password" : req.body.password
    //   }
    //   global.user_file[posi - 1]= newUser
    //   console.log("usuario actualizado: " + newUser);
    //   res.send(newUser);
    //   //res.send({"msg":"POST exitoso"})
    // });

//DELETE users
app.delete(global.URL_BASE + 'users/:id', users_contraller.deleteUserId);
// function(req,res){
//   console.log('Delete de users');
//   //vamos a usar el splice
//   let pos = req.params.id;
//   user_file.splice(pos - 1,1);
//   //if user_file[pos] == undefined
//   //   {"msg":"Usuario no encontrado"}
//   //else {
//     res.send({"msg":"Usuario eliminado"});
//   //}
//
// });

//Login *******
//app.post(global.URL_BASE + 'login', users_contraller.loginUser);
//**************
// function(req,res){
//   console.log('Login');
//   console.log(req.body.email);
//   console.log(req.body.password)
//   let tam = user_file.length;
//   let i= 0;
//   let encontrado = false;
//   let email = req.body.email;
//   let password = req.body.password;
//
//   while ((i < global.user_file.length) && !encontrado){
//      if (user_file[i].email == email && user_file[i].password == password)
//      {
//        idusuario = user_file[i].id;
//        user_file[i].logged = true;
//        encontrado = true;
//   //   //  break;
//      }
//      i++;
//      if (encontrado)
//        res.send({"encontrado":"si","id":i});
//      else {
//        res.send({"encontrado": "no"});
//      }
//    }
// });

//Escribir datos en fichero persistente
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 };





//Logout
app.post(global.URL_BASE + 'logout/:id', users_contraller.logoutUser);
// function(req,res){
//   console.log('Logout');
//   console.log(req.params.id);
//   let i= 0;
//   let encontrado = false;
//   let pos = req.params.id - 1;
//
//
//   if (global.user_file[pos].logged == true) {
//     delete user_file[pos].logged;
//     //if (user_file[pos].logged = false){
//        res.send({"logout correcto":"si","id":req.params.id});
//     }
//   else {
//         res.send({"logout incorrecto":"si","id":req.params.id});
//   }
//
// });

// GET users consumiendo API REST de mLab
app.get(global.URL_BASE + 'usersLab',
 function(req, res) {
   console.log("GET /techu-peru/v1/users");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// GET users consumiendo API REST de mLab on id
app.get(global.URL_BASE + 'usersLab/:id',
 function(req, res) {
   console.log("GET /techu-peru/v1/usersLab/:id");
   console.log(req.params.id);
   var id = req.params.id;
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString1 = 'q={"id":'+ id +'}&';
   var queryString2 = 'f={"_id":0}&';
   httpClient.get('user?' + queryString1 + queryString2 + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// GET users/:id/accounts consumiendo API REST de mLab
app.get(global.URL_BASE + 'usersLab/:id/accounts',
 function(req, res) {
   console.log(global.URL_BASE);
   console.log("request.params.id: " + req.params.id);
   var id = req.params.id;
   var queryStringID = 'q={"id_user":' + id +'}&';
   var queryString = 'f={"_id":0}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
  console.log(queryStringID);
   httpClient.get('accounts?' + queryString + queryStringID + apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log('error ' + err);
       console.log('requestMLab ' + respuestaMLab);
       console.log('body.length ' + body.length);
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// Metodo POST Login
app.post(global.URL_BASE + 'Login',
 function(req, res) {
   console.log('POST /techu-peru/v1/login');
   let email = req.body.email;
   let pass = req.body.password;
   let queryString = 'q={"email": ' + email + ' "password": ' + pass +'}&';
   let limfilter = 'l=1&';
   //let clienteMlab = requestJSON.createClient(baseMLabURL);
   var httpClient = requestJSON.createClient(baseMLabURL);
  console.log(queryString);
   httpClient.get('user?' + queryString + limfilter + apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log('error ' + err);
       console.log('requestMLab ' + respuestaMLab);
       console.log('body.length ' + body.length);
       var response = {};
       if(err) {
         if (body.length == 1){
            let login = '{$set:{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apikeyMLab, JSON.parse(login),
            function(errPut, resPut, bodyPut) {
                res.send({'msg': 'login correcto', 'user': body[o].email, 'userid': body[0].id, 'name': body[0].first_name});
            });
        }
        else {
           res.status(404).send({"msg" : "usuario no valido"});
         }
       }
       else{
         res.status(500).send({"msg" : "Error en peticion a mLab."});
       }
     });
});

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
